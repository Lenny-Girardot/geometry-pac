#include <SFML/Graphics.hpp>
#include <fstream>

using namespace sf;

Font chargeFont(char nom[ ]);
void afficheMenu(RenderWindow *fenetre, Sprite spFond, Sprite spBack, int *phase);
void selectionPseudo ( RenderWindow *fenetre, Sprite spFond, Sprite spBack, char pseudo[ ], int *phase);
void affiScore( RenderWindow *fenetre, Sprite spFond, Sprite spBack, char pseudoX[ ], int scoreX, int *phase, int *jouer);
void afficheOr( RenderWindow *fenetre, int *phase, int *score );
void lanceJeu(RenderWindow *fenetre, int *phase, int *score);
