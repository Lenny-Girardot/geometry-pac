#include "fonction.h"

#define HAUTEURFENETRE 600
#define LARGEURFENETRE 1000
#define HAUTEURBOUTON 100
#define LARGEURBOUTON 300
#define HAUTEURCREDIT 57
#define LARGEURCREDIT 100
#define POS_X_PERSO 100
#define PAS_PERSO 2
#define HAUTEURPERSO 200
#define LARGEURREGLE 150
#define HAUTEURREGLE 50
#define IMG_FOND "image/fond.png"
#define IMG_CIEL "image/new.png"

int main()
{
    char pseudo[20]; // Pseudo du jouer demande dans selection Pseudo
    int jouer = 1;

    Texture texFond, texBack;
    Sprite spFond, spBack;

    texBack.loadFromFile(IMG_CIEL);
    texFond.loadFromFile(IMG_FOND);

    spFond.setTexture(texFond);
    spBack.setTexture(texBack);

    spBack.setPosition(0,0);
    spFond.setPosition(0,0);

    RenderWindow fenetre(VideoMode(LARGEURFENETRE,HAUTEURFENETRE), "GeometryPac");

    do
    {
        int score = 0;
        int phase = 1; // Etape du code (menu/saisie/jeu...)

        // Lancement du menu
        afficheMenu(&fenetre, spFond, spBack, &phase);

        // Lancement de la saisie de pseudo si le joueur appuie sur jouer
        selectionPseudo(&fenetre, spFond, spBack, pseudo, &phase);

        printf("Votre pseudo est : %s\n",pseudo);

        // On affiche le score du joueur
        lanceJeu(&fenetre, &phase, &score);

        // Compare le score joueur aux 5 premiers, le remplace si necessaire et affiche les 5 premiers scores
        affiScore(&fenetre, spFond, spBack, pseudo, score, &phase, &jouer);
        // SCORE A METTRE EN VARIABLE

    }
    while(jouer == 1);

    return 0;
}



