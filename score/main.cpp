#include <SFML/Graphics.hpp>
#include <fstream>
#include <iostream>

using namespace sf;


// NE PAS COPIER (DEJA PRESENTE DANS MAIN)
Font chargeFont(char nom[ ]) {
    Font font;
    if(!font.loadFromFile(nom))
        printf("Erreur de chargement de la police");
    return font;
}
// NE PAS COPIER (DEJA PRESENTE DANS MAIN)


void affiScore( RenderWindow *fenetre, char pseudoX[ ], int scoreX ) {
    // Chemin du flux de lecture
    std::ifstream entree("score.txt");

    // Lecture du fichier et comparaison des scores
    int resultat, position = 5,trouve = 0, nbreecriture = 0;
    char texteRes[75];
    char reecriture[200] = " "; // On donne une valeure de base pour pas que ca psoe de problemes avec le sprintf
    Text texteScores, texteResultat;
    Font font = chargeFont("arial.ttf");
    texteScores.setFont(font);
    texteResultat.setFont(font);
    texteResultat.setPosition(50,450);
    texteScores.setPosition(50,50);
    std::string ligne;

    if(entree) // Fichier ouvert avec succes
    {
        int i;
        for(i=0;i<5;i++) { // Tant que la ligne n'est pas vide on lit
            std::string pseudo;
            entree >> pseudo;
            int score;
            entree >> score;
            position--;
            if (scoreX>score && trouve != 1) {
                resultat = (5-position);
                trouve = 1;
                sprintf(reecriture, "%s\n%s %i", reecriture, pseudoX, scoreX);
                nbreecriture++;
            }

            if (nbreecriture < 5) {
                sprintf(reecriture, "%s\n%s %i", reecriture, pseudo.c_str(), score);
                nbreecriture++;
            }
            else if(trouve == 0 && nbreecriture >= 5) {
                resultat = 99; // La valeur n'est pas trouve mais ne se situe pas dans le top 5
            }
        }
    printf("Vous etes %i", resultat);
    }
    else // Probleme avec le fichier texte
    {
        printf("Probleme avec le fichier score.txt (lecture)");
    }

    // Chemin du flux d'ecriture
    std::ofstream sortie("score.txt");

    // Ecriure des nouveaux 5 premiers dans le fichier texte
    sortie << reecriture << std::endl;

    texteScores.setString(reecriture);
    if(resultat <= 5 && resultat > 0)
        sprintf(texteRes,"Felicitation, vous etes %i", resultat);
    else
       sprintf(texteRes,"Malheureusement, vous n'etes pas dans le top 5... Rejouez !");
    printf("Resultat : %i\n", resultat);
    printf("posiion : %i\n", position);
    texteResultat.setString(texteRes);
    fenetre->draw(texteResultat);
    fenetre->draw(texteScores);
    fenetre->display();

    /* Variables :
    resultat = position du joueur si il est dans le top 5 sinon = 99
    reecriure = texte qui est reecrit dans le fichier texte (modifie si joueur est dans top5)
    */
}



int main()
{
    RenderWindow fenetre(VideoMode(900,600),"fenetre");
    affiScore(&fenetre, "Damien", 97);

    while (fenetre.isOpen())
    {
        // Process events
        Event event;
        while (fenetre.pollEvent(event))
        {
            // Close window : exit
            if (event.type ==  Event::Closed)
                fenetre.close();
        }
    }

    return 0;
}
