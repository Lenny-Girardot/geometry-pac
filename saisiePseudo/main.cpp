#include <SFML/Graphics.hpp>
using namespace sf;

Font chargeFont(char nom[ ]) {
    Font font;
    if(!font.loadFromFile(nom))
        printf("Erreur de chargement de la police");
    return font;
}

void selectionPseudo ( RenderWindow *fenetre, char pseudo[ ]) {
    char pseudoVariable[20] = "";
    int nbLettre = 0, saisie=1, i=0;
    fenetre->clear();
    Text textePseudo;
    Font font = chargeFont("arial.ttf");
    textePseudo.setFont(font);
    textePseudo.setPosition(100,200);
    do {
        Event evt;
        while (fenetre->pollEvent(evt)) {
                if (evt.type == sf::Event::Closed)
                    fenetre->close();
                if (evt.type == Event::TextEntered) {
                    if(isalpha(evt.text.unicode) && nbLettre < 19)
                    {
                        pseudoVariable[i] = evt.text.unicode;
                        i++;
                        pseudoVariable[i] = '\0';
                        textePseudo.setString(pseudoVariable);
                        printf("%s\n",pseudoVariable); //TEST
                        nbLettre++;
                    }
                }
                if ((evt.type == Event::KeyPressed && evt.key.code == Keyboard::Return) || nbLettre == 19){ // SI ON APPUIE SUR ENTRER OU SI PSEUDO>20 LETTRES
                    saisie = 0;
                }
        }
        fenetre->draw(textePseudo);
        fenetre->display();
    } while (saisie == 1);
    printf("Saisie terminee\n");
    for(int i=0;i<sizeof(pseudoVariable);i++) {
        pseudo[i] = pseudoVariable[i];
    }
}

int main()
{
    char pseudo[20];

    RenderWindow fenetre(sf::VideoMode(800, 600), "SFML window");
    selectionPseudo(&fenetre, pseudo);

    while (fenetre.isOpen())
    {
        sf::Event event;
        while (fenetre.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                fenetre.close();
        }
    }
    return 0;
}
